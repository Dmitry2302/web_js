import Pokemon from "./pokemon.js"
import {attack, init, action} from "./methods.js"

const $btn = document.getElementById('btn-kick');
const $btnOth = document.getElementById('btn-other');

let regAttackAmount = 5;
let criticalAttackAmount = 5;

const character = new Pokemon({
    name: 'Pikachu',
    defaultHP: 100,
    damageHP: 100,
    selectors: 'character'
})

const enemy = new Pokemon ({
    name: 'Charmander',
    defaultHP: 100,
    damageHP: 100,
    selectors: 'enemy'
})

console.log(character);
console.log(enemy);

init();


document.addEventListener('click', (e) => {
    if (e.target.id == "btn-kick") {    
        regAttackAmount--;
        document.getElementById("shotsReg").innerHTML = regAttackAmount;
        console.log(regAttackAmount, "SMOTRI")
    }
    if (e.target.id == "btn-other") { 
        criticalAttackAmount--;
        document.getElementById("shotsCrit").innerHTML = criticalAttackAmount;
    }
})

const {name, ...rest} = character;
const {name: nameEnemy, ...restEnemy} = enemy;
console.log(name, rest);
console.log(nameEnemy, restEnemy);


const al = action($btn);
const ar = action($btnOth);
$btn.addEventListener('click', () => {
    al();
    attack(character, 20, enemy, 20);
    console.log('Kick!')
});

$btnOth.addEventListener('click', () => {
    ar();
    attack(character, 50, enemy, 25);
    console.log('Critical damage')
}); 